//
//  mapViewController.swift
//  screenTest
//
//  Created by Muhammad Arif Rahman on 11/3/16.
//  Copyright © 2016 suitmedia. All rights reserved.
//

import UIKit
import ImageSlideshow
import GoogleMaps


class mapViewController: UIViewController {
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return .portrait
        }
    }
    
    @IBOutlet weak var imageSlide: ImageSlideshow!
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate?
    
    let localSource = [ImageSource(imageString: "img1")!, ImageSource(imageString: "img2")!, ImageSource(imageString: "img3")!, ImageSource(imageString: "img4")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageSlide.backgroundColor = UIColor.white
        imageSlide.slideshowInterval = 5.0
        imageSlide.pageControlPosition = PageControlPosition.underScrollView
        imageSlide.pageControl.currentPageIndicatorTintColor = UIColor.lightGray;
        imageSlide.pageControl.pageIndicatorTintColor = UIColor.black;
        imageSlide.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        // try out other sources such as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        imageSlide.setImageInputs(localSource)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(mapViewController.didTap))
        imageSlide.addGestureRecognizer(recognizer)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTap() {
        imageSlide.presentFullScreenController(from: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
