//
//  eventTableViewController.swift
//  screenTest
//
//  Created by Muhammad Arif Rahman on 9/6/16.
//  Copyright © 2016 suitmedia. All rights reserved.
//

import UIKit

class eventTableViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!

    var events = [Event]()
    var valueToPass:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let rightAddBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_newMediaArticle_normal.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(eventTableViewController.addTapped))
        // 2
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(eventTableViewController.searchTapped))
        // 3
        self.navigationItem.setRightBarButtonItems([rightAddBarButtonItem,rightSearchBarButtonItem], animated: true)
        // 4 
        let leftBackBarButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_backArticle_normal.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(eventTableViewController.backTapped))
        // 5
        self.navigationItem.setLeftBarButtonItems([leftBackBarButtonItem], animated: true)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let rightAddBarButtonItem:UIBarButtonItem = UIBarButtonItem(title: "Add", style: UIBarButtonItemStyle.plain, target: self, action: #selector(eventTableViewController.addTapped))
        // 2
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(eventTableViewController.searchTapped))
        // 3
        self.navigationItem.setRightBarButtonItems([rightAddBarButtonItem,rightSearchBarButtonItem], animated: true)
        // Load the sample data.
        loadSampleEvents()
    }
    
    func searchTapped(sender:UIButton) {
        print("search pressed")
    }
    // 5
    func addTapped (sender:UIButton) {
        if let resultController = storyboard!.instantiateViewController(withIdentifier: "MapVC") as? mapViewController {
            let navController = UINavigationController(rootViewController: resultController) // Creating a navigation controller with resultController at the root of the navigation stack.
            self.present(navController, animated:true, completion: nil)
        }
        print("add pressed")
    }
    func backTapped (sender:UIButton) {
        print("back pressed")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "segueCell") {
            
            // initialize new view controller and cast it as your view controller
            let MVController = segue.destination as! menuViewController
            // your new view controller should have property that will store passed value
            MVController.passedValue = valueToPass
        }
        
    }
    
    func loadSampleEvents() {
        let photo1 = UIImage(named: "event1")!
        let event1 = Event(nama: "Lahiran", photo: photo1, tanggal: "2 Oktober 2016")!
        
        let photo2 = UIImage(named: "event2")!
        let event2 = Event(nama: "Nikahan", photo: photo2, tanggal: "3 Oktober 2016")!
        
        let photo3 = UIImage(named: "event3")!
        let event3 = Event(nama: "Akikahan", photo: photo3, tanggal: "8 Oktober 2016")!
        
        let photo4 = UIImage(named: "event4")!
        let event4 = Event(nama: "Sunatan", photo: photo4, tanggal: "24 Oktober 2016")!
        
        events += [event1, event2, event3, event4]
    }

}

extension eventTableViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "eventTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! eventTableViewCell
        
        // Fetches the appropriate event for the data source layout.
        let event = events[(indexPath as NSIndexPath).row]
        
        cell.namaLabel.text = event.nama
        cell.photoImageView.image = event.photo
        cell.tanggalLabel.text = event.tanggal
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Get Cell Label
        let indexPath = tableView.indexPathForSelectedRow;
        _ = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
        let cellIdentifier = "eventTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath!) as! eventTableViewCell
        
        performSegue(withIdentifier: "segueMenu", sender: self)
        let event = events[indexPath!.row]
        let event = events[(indexPath! as NSIndexPath).row]
        cell.namaLabel.text = event.nama
        cell.photoImageView.image = event.photo
        cell.tanggalLabel.text = event.tanggal
        valueToPass = cell.textLabel!.text
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "segueCell") {
            
            // initialize new view controller and cast it as your view controller
            let MVController = segue.destination as! menuViewController
            // your new view controller should have property that will store passed value
            MVController.passedValue = valueToPass
        }
        
    }
    
    func loadSampleEvents() {
        let photo1 = UIImage(named: "event1")!
        let event1 = Event(nama: "Lahiran", photo: photo1, tanggal: "2 Oktober 2016")!
        
        let photo2 = UIImage(named: "event2")!
        let event2 = Event(nama: "Nikahan", photo: photo2, tanggal: "3 Oktober 2016")!
        
        let photo3 = UIImage(named: "event3")!
        let event3 = Event(nama: "Akikahan", photo: photo3, tanggal: "8 Oktober 2016")!
        
        let photo4 = UIImage(named: "event4")!
        let event4 = Event(nama: "Sunatan", photo: photo4, tanggal: "24 Oktober 2016")!
        
        events += [event1, event2, event3, event4]
    }

}
