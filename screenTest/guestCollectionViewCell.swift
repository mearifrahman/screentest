//
//  guestCollectionViewCell.swift
//  screenTest
//
//  Created by Muhammad Arif Rahman on 9/6/16.
//  Copyright © 2016 suitmedia. All rights reserved.
//

import UIKit

class guestCollectionViewCell: UICollectionViewCell {
    @IBOutlet var name: UILabel!
    @IBOutlet var date: UILabel!
}
