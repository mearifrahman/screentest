//
//  eventTableViewCell.swift
//  screenTest
//
//  Created by Muhammad Arif Rahman on 9/6/16.
//  Copyright © 2016 suitmedia. All rights reserved.
//

import UIKit

class eventTableViewCell: UITableViewCell {
    
    //MARK Properties
    @IBOutlet weak var namaLabel: UILabel!
    @IBOutlet weak var tanggalLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Initialization Code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
