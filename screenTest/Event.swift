//
//  Event.swift
//  screenTest
//
//  Created by Muhammad Arif Rahman on 9/6/16.
//  Copyright © 2016 suitmedia. All rights reserved.
//

import UIKit

class Event {
    // MARK: Properties
    
    var nama: String
    var photo: UIImage!
    var tanggal: String
    
    
    init?(nama: String, photo: UIImage?, tanggal: String) {
        // Initialize stored properties.
        self.nama = nama
        self.photo = photo
        self.tanggal = tanggal
        
        // Initialization should fail if there is no name or if the rating is negative.
        if nama.isEmpty || tanggal.isEmpty {
            return nil
        }
    }
}