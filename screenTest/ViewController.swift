//
//  ViewController.swift
//  screenTest
//
//  Created by Muhammad Arif Rahman on 9/5/16.
//  Copyright © 2016 suitmedia. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
class ViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var textField: UITextField! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard)))
        // Do any additional setup after loading the view, typically from a nib.
        textField.delegate=self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard() {
        textField.resignFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let palindrome = textField.text!
        let reverse = String(palindrome.characters.reversed())
        let DestViewController: menuViewController = segue.destination as! menuViewController
        
        if (reverse == palindrome) {
            let alert = UIAlertController(title: "Success", message: "\(DestViewController.LabelText) is a palindrome", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                //let menuVCD = self.storyboard?.instantiateViewController(withIdentifier: "menuVC")
                let DestViewController: menuViewController = segue.destination as! menuViewController
                DestViewController.LabelText = palindrome
                self.present(DestViewController, animated: true, completion: nil)
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            //print("\(DestViewController.LabelText) is a palindrome")
        } else {
            let alert = UIAlertController(title: "Success", message: "\(DestViewController.LabelText) is not a palindrome", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                //let menuVCD = self.storyboard?.instantiateViewController(withIdentifier: "menuVC")
                let DestViewController: menuViewController = segue.destination as! menuViewController
                DestViewController.LabelText = palindrome
                self.present(DestViewController, animated: true, completion: nil)
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            //print("\(DestViewController.LabelText) is not a palindrome")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DestViewController: menuViewController = segue.destination as! menuViewController
        DestViewController.LabelText = textField.text!
        
        let reverse = String(DestViewController.LabelText.characters.reversed())
        
        if (reverse == DestViewController.LabelText) {
            print("\(DestViewController.LabelText) is a palindrome")
        } else {
            print("\(DestViewController.LabelText) is not a palindrome")
        }
        
    }
    
}
