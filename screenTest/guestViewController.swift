//
//  guestViewController.swift
//  screenTest
//
//  Created by Muhammad Arif Rahman on 9/6/16.
//  Copyright © 2016 suitmedia. All rights reserved.
//

import UIKit

class guestViewController: UIViewController {
    
    
    @IBOutlet var collectionview: UICollectionView!
    
    struct name
    {
        var name:String
        var date:String
        init(date:String, name:String)
        {
            self.name = name
            self.date = date
        }
    }
    
    var Data:Array<  name > = Array < name >()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionview.delegate = self
        collectionview.dataSource = self
        
//        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top:1,left:10,bottom:10,right:10)
//        layout.minimumInteritemSpacing = 5
//        layout.minimumLineSpacing = 10
//        
//        collectionview.collectionViewLayout = layout
    }
    
    
    
    
    func extract_json(_ data:NSString)
    {
        var parseError: NSError?
        let jsonData:Foundation.Data = data.data(using: String.Encoding.ascii.rawValue)!
        let json: AnyObject?
        do {
            json = try JSONSerialization.jsonObject(with: jsonData, options: []) as AnyObject
        } catch let error as NSError {
            parseError = error
            json = nil
        }
        if (parseError == nil)
        {
            if let names_list = json as? NSArray
            {
                for i in (0 ..< names_list.count )
                {
                    if let name_obj = names_list[i] as? NSDictionary
                    {
                        if let name_name = name_obj["name"] as? String
                        {
                            if let name_date = name_obj["birthdate"] as? String
                            {
                                let add_it = name(date: name_date, name: name_name)
                                Data.append(add_it)
                            }
                        }
                    }
                }
            }
        }
        do_refresh();
    }
    
    
    func do_refresh()
    {
        DispatchQueue.main.async(execute: {
            self.collectionview.reloadData()
            return
        })
    }
    
    func get_data_from_url(_ url:String)
    {
        
        
        let session = URLSession.shared
        let urlString = "http://dry-sierra-6832.herokuapp.com/api/people"
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        let dataTask = session.dataTask(with: request, completionHandler: { (data:Foundation.Data?, response:URLResponse?, error:NSError?) -> Void in
            print("done, error: \(error)")
            } as! (Data?, URLResponse?, Error?) -> Void)
        dataTask.resume()
        
    }
}

extension guestViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        } as! (Data?, URLResponse?, Error?) -> Void) 
        dataTask.resume()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(section)
        return 8
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GuestCell", for: indexPath) as! guestCollectionViewCell
        cell.name.text = "nama \(indexPath.row)"
        return cell
    }
}

